"""
Ce programme prend en entrée un fichier csv et le transforme en un autre fichier
csv en changeant le délimiteur et le noms des lignes.

"""

__author__ = "Jérémy Dufossé"

import csv
import os

rows=[]

def read_file(file_input):
    """
    Lis un fichier csv avec le délimiteur '|'

    Paramètre :
    -----------
    file_input : str
        Le nom du fichier à lire
    """
    with open(file_input,newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter='|')
        for line_content in reader:
            rows.append(line_content)

def write_file(file_output):
    """
    Créer un fichier csv avec le délimiteur ';'

    Paramètre :
    -----------
    file_output : str
        Le nom du fichier à créer
    """
    with open(file_output,'w',newline='') as csvfile:
        field_names=['adresse_titulaire','nom','prenom','immatriculation','date_immatriculation','vin','marque','denomination_commerciale','couleur',
                     'carroserie','categorie','cylindree','energie','places','poids','puissance','type','variante','version']
        writer = csv.DictWriter(csvfile, field_names,delimiter=";")
        writer.writeheader()
        for row in rows:
            liste_caract=row['type_variante_version'].split(', ')
            writer.writerow({
                'adresse_titulaire': row['address'],
                'nom': row['name'],
                'prenom': row['firstname'],
                'immatriculation': row['immat'],
                'date_immatriculation': row['date_immat'],
                'vin': row['vin'],
                'marque': row['marque'],
                'denomination_commerciale': row['denomination'],
                'couleur': row['couleur'],
                'carroserie': row['carrosserie'],
                'categorie': row['categorie'],
                'cylindree': row['cylindree'],
                'energie': row['energy'],
                'places': row['places'],
                'poids': row['poids'],
                'puissance': row['puissance'],
                'type': liste_caract[0],
                'variante': liste_caract[1],
                'version': liste_caract[2],
                            })

def check_in():
    """
    Récupére le nom du fichier csv a lire, vérifie qu'il existe et envoie le
    nom du fichier vers la fonction "read_file"

    """
    file_input=input("Saisissez le nom du fichier CSV à modifier : (X.csv) ")
    if(os.path.isfile(file_input)==True):
        read_file(file_input)
    else:
        print("Erreur: fichier introuvable")
        exit(1)


def check_out():
    """
    Récupére le nom du fichier csv a créer, et envoie le
    nom du fichier vers la fonction "write_file"

    """
    file_output=input("Saisissez le nom du fichier CSV qui sera créer : (X.csv) ")
    write_file(file_output)


if __name__ == '__main__':
    check_in()
    check_out()
    exit(0)


