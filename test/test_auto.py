import unittest
import Auto_csv
import csv
import os


class testminiprojetauto(unittest.TestCase):
    """
    Class qui test les fonction de lecture et d'écriture sur un fichier csv
    """
    def testread(self):
        fichier = open(".\auto.csv", newline='')
        file_input = csv.DictReader(fichier, delimiter='|')
        buffer = file_input
        self.assertEqual(Auto_csv.lire_entree(file_input), buffer)
        fichier.close()

    def testwrite(self):
        fichier = open(".\auto.csv", newline='')
        file_input = csv.DictReader(fichier, delimiter='|')
        buffer = file_input

        sortie = open(".\sortie.txt",newline='')
        self.assertEqual(Auto_csv.gestion_sortie(buffer),sortie)